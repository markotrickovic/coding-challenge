// ReadFileFromArgs.cpp : This file contains the 'main' function. Program execution begins and ends there.
// ucitati n osoba i ispisati ih sortirane alfabetskim redosledom po prezimenu

#include <iostream>

typedef struct {
    char ime[20];
    char prezime[20];
} Osoba;

FILE* f;

void sort_str(Osoba niz_str[], int n)
{
    int i, j;
    for(i=0; i<n-1; i++)
        for(j=i+1; j<n; j++)
            if (_strcmpi(niz_str[i].prezime, niz_str[j].prezime) > 0) {
                Osoba tmp = niz_str[i];
                niz_str[i] = niz_str[j];
                niz_str[j] = tmp;
            }

    for (int i = 0; i < n; i++) {
        std::cout << niz_str[i].ime << " " << niz_str[i].prezime << std::endl;
    }
}

char* vrati_pok(char* s, char c)
{
    char* pok = s;
    while (*pok)
        if (*pok++==c) return pok;
    return NULL;
}

void getline(char *buffer)
{
    unsigned int i = 0, x;
    while ((x = getc(f)) != EOF && x != '\n') {
        buffer[i++] = (char)x;
    }
    buffer[i] = '\0';
}

int getlinecount()
{
    int br = 0, x;
    while ((x = getc(f)) != EOF) {
        if (x == '\n') br++;
    }
    return br;
}

int main(int argc, char** argv)
{
    errno_t err;
    Osoba sveosobe[100];
    char red[100];

    if (argc <= 1) {
        fprintf(stderr, "Argument sa putanjom do fajla nije unet");
        exit(EXIT_FAILURE);
    }

    err = fopen_s(&f, argv[1], "r");
    if (err == 0)
    {
        printf("Fajl '%s' je otvoren\n", argv[1]);
    }
    else
    {
        printf("Fajl '%s' nije otvoren\n", argv[1]);
    }

    const int brOsoba = getlinecount();
    fseek(f, 0, SEEK_SET);
    for (int i = 0; i < brOsoba; i++) {
        getline(red);
        int duzina = strlen(red);
        char* prezime = vrati_pok(red, ' ');
        int razlika = duzina - strlen(prezime) - 1;
        char ime[20];
        strncpy_s(ime, red, razlika);
        strcpy_s(sveosobe[i].ime, ime);
        strcpy_s(sveosobe[i].prezime, prezime);
        //std::cout << sveosobe[i].ime << " " << sveosobe[i].prezime << std::endl;
    }

    sort_str(sveosobe, brOsoba);

    if (f)
    {
        err = fclose(f);
        if (err == 0)
        {
            printf("Fajl '%s' je zatvoren\n", argv[1]);
        }
        else
        {
            printf("Fajl '%s' nije zatvoren\n", argv[1]);
        }
    }

    int brzatvorenih = _fcloseall();
    printf("Broj fajlova zatvorenih sa _fcloseall: %u\n", brzatvorenih);
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
