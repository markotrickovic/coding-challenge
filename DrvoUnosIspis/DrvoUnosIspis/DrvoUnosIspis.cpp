// DrvoUnosIspis.cpp : This file contains the 'main' function. Program execution begins and ends there.
// program koji omogucava unos podataka u drvo i njihov ispis

#include <stdio.h>
#include <stdlib.h>
typedef struct Element {
    int podatak;
    struct Element* _levi, * _desni;
} Element;
Element* ubaci(Element* novi, Element* el) {
    if (el == NULL) {
        novi->_levi = NULL;
        novi->_desni = NULL;
        return novi;
    }
    else {
        if (novi->podatak < el->podatak)
            el->_levi = ubaci(novi, el->_levi);
        else el->_desni = ubaci(novi, el->_desni);
        return el;
    }
}
void ispisi(Element* drvo) {
    if (drvo != NULL) {
        ispisi(drvo->_levi);
        printf("%d\n", drvo->podatak);
        ispisi(drvo->_desni);
    }
}
void obrisi(Element* drvo) {
    /* Pogledati zadatak broj 21 */
}
int main()
{
    int broj;
    Element* koren = NULL, * novi;
    printf("Unesite brojeve koje smestamo u drvo: \n");
    while (1) {
        scanf_s("%d", &broj);
        if (broj == 0) break;
        novi = (Element*)malloc(sizeof(Element));
        novi->podatak = broj;
        koren = ubaci(novi, koren);
    }
    printf("Sadrzaj drveta: \n");
    ispisi(koren);
    obrisi(koren);
    return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
