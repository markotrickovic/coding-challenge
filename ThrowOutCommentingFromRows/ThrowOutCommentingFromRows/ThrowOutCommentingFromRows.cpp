// ThrowOutCommentingFromRows.cpp : This file contains the 'main' function. Program execution begins and ends there.
// Throw out commenting from row which are inserted by user.
// Assume one comment in one line.

#include <stdio.h>
#include <string.h>
void izbaci(char* s, char* tmp)
{
	int duz = strlen(s), t;
	const char* poc = "/*", *kraj = "*/";
	char* poz_poc = strstr(s, poc), * poz_kraj = strstr(s, kraj);
	tmp[0] = '\0';
	if (poz_poc == NULL || poz_kraj == NULL) {
		strcpy_s(tmp, 100, s);
		return;
	}
	strncat_s(tmp, 100, s, poz_poc - s);
	strcat_s(tmp, 100, poz_kraj + 2);
}
int main()
{
	char red[100];
	char nred[100];
	strcpy_s(red, "int i; /* komentar */");
	//scanf_s("%s", &red);
	izbaci(red, nred);
	printf("%s\n", red);
	printf("%s", nred);
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
