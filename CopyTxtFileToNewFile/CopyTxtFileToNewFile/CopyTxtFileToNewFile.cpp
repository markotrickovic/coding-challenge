// CopyTxtFileToNewFile.cpp : This file contains the 'main' function. Program execution begins and ends there.
// prepisuje sadrzaj fajla f u fajl g tako sto svaki red ispisuje u fajl g zajedno sa njegovim rednim brojem

#include <iostream>

#define MAX_LINES 100

typedef struct {
    char ime[50];
    char prezime[50];
    int godiste;
} Osoba;

FILE* f;

char* vrati_pok(char* s, char c)
{
    char* pok = s;
    while (*pok)
        if (*pok++ == c) return pok;
    return NULL;
}

void getline(char* buffer)
{
    unsigned int i = 0, x;
    while ((x = getc(f)) != EOF && x != '\n') {
        buffer[i++] = (char)x;
    }
    buffer[i] = '\0';
}

int getlinecount()
{
    int br = 0, x;
    while ((x = getc(f)) != EOF) {
        if (x == '\n') br++;
    }
    return br;
}

Osoba split(char* s, char c)
{
    Osoba o;
    //char* broj = vrati_pok(s, c);
    char* ime = vrati_pok(s, c);
    char* prezime = vrati_pok(ime, c);
    char* godiste = vrati_pok(prezime, c);
    int duzina_ime = (int)prezime-1 - (int)ime;
    int duzina_prezime = (int)godiste-1 - (int)prezime;
    strncpy_s(o.ime, ime, duzina_ime);
    strncpy_s(o.prezime, prezime, duzina_prezime);
    o.godiste = atoi(godiste);
    return o;
}

int main(int argc, char** argv)
{
    errno_t err;
    char red[100] , nred[100];
    Osoba sveosobe[100];

    if (argc <= 1) {
        fprintf(stderr, "Argument sa putanjom do fajla nije unet");
        exit(EXIT_FAILURE);
    }

    err = fopen_s(&f, argv[1], "r");
    if (err == 0)
    {
        printf("Fajl '%s' je otvoren\n", argv[1]);
    }
    else
    {
        printf("Fajl '%s' nije otvoren\n", argv[1]);
    }

    const int brOsoba = getlinecount();
    fseek(f, 0, SEEK_SET);
    for (int i = 0; i <= brOsoba && i < MAX_LINES; i++) {
        getline(red);
        sveosobe[i] = split(red, ' ');
        if (sveosobe[i].godiste > 1980)
            std::cout << sveosobe[i].ime << " " << sveosobe[i].prezime << " " << sveosobe[i].godiste << std::endl;
    }

    if (f)
    {
        err = fclose(f);
        if (err == 0)
        {
            printf("Fajl '%s' je zatvoren\n", argv[1]);
        }
        else
        {
            printf("Fajl '%s' nije zatvoren\n", argv[1]);
        }
    }

    int brzatvorenih = _fcloseall();
    printf("Broj zatvorenih fajlova sa _fcloseall: %u\n", brzatvorenih);
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
