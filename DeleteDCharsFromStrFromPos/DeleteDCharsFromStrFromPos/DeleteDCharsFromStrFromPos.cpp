// DeleteDCharsFromStrFromPos.cpp : This file contains the 'main' function. Program execution begins and ends there.
// Write del function which from str deletes d characters, starting
// from the pos and it's testing it in the main program. During deletion
// take care if the string has d characters starting from the pos 

#include <stdio.h>
#include <string.h>
// function: del
// args: str, d, pos
// str - strng, d - number of characters
// pos - starting position
void del(char* str,
		 unsigned int d,
		 unsigned int pos)
{
	int duzina = strlen(str);
	int ost = duzina - pos - d;	// remainder d chars
	if ((d + pos) > duzina) {
		fprintf(stderr, "Nema dovoljno znakova %d %d\n", pos, ost);
		return;
	}
	/*
	pos -> d1 d2 d3 d4 s34 s35 s36 \0
		 - d1 d2 d3 d4
	pos -> s34 s35 s36 \0
	*/
	for (int i = pos; i <= pos + ost; i++)
		str[i] = str[i + d];
}
int main()
{
	char ime[20] = "Marko Trickovic";
	del(ime, 6, 4);
	printf("%s", ime);
	// output: Markkovic
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
