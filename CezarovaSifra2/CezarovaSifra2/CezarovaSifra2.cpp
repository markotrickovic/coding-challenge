// CezarovaSifra2.cpp : This file contains the 'main' function. Program execution begins and ends there.
// ulaz sa tastature ispisuje na ekranu kao tekst �ifrovan Cezarovom �ifrom

#include <iostream>

int main()
{
	int ch, i = 0;
	char red[100];
	while ((ch = getchar()) != EOF) {
		red[i++] = ch;
	}
	red[i] = '\0';
	int j = i;
	for (int i = 0; i < j; i++) {
		if (red[i] > 'A' && red[i] <= 'Z') putchar('A' + ('A' + (ch - 'A' + 3) % 26));
		else if (red[i] >= 'a' && red[i] <= 'z') putchar('a' + (red[i] - 'a' + 3) % 26);
	}
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
