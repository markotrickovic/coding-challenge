// ReadArrayOfPeople.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
typedef struct {
    char ime[50];
    char prezime[50];
    int godiste;
} Osoba;
void ispisi_osobu(Osoba o) {
    printf("Ime: %s Prezime: %s Godiste: %d\n",
        o.ime, o.prezime, o.godiste);
}
int main(int argc, char **argv)
{
    FILE* f;
    int br = 0;
    Osoba o[100];
    if (argc < 2) {
        fprintf(stderr, "Nema dovoljno argumenata!");
        exit(EXIT_FAILURE);
    }
    if ((fopen_s(&f, argv[1], "r")) == 0) {
        printf("Fajl '%s' je otvoren\n", argv[1]);
        exit(EXIT_FAILURE);
    }
    else
    {
        printf("Fajl '%s' nije otvoren\n", argv[1]);
    }
    if (fscanf_s(f, "%s %s %d", &o[br].ime, &o[br].prezime, &o[br].godiste) > 0)
        ispisi_osobu(o[br]);
    else
        printf("podaci o osobi nisu dobro formatirani");

    fclose(f);
    exit(EXIT_SUCCESS);
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
